#include "Vector.h"
#include<iostream>

using namespace std;

int main()
{
	Vector vector(1, 1, 1, true);
	cout << "The cartesian vector is: " << endl;
	vector.PrintCoordinates();

	double scalar = 2;
	Vector multipledVector = vector.MultiplyScalar(scalar);
	cout << "The vector multiplied by " << scalar << " is:" << endl;
	multipledVector.PrintCoordinates();
	
	Vector vectorPolar(1, 1, 1, false);
	cout << "The polar vector is: " << endl;
	vectorPolar.PrintCoordinates();

	Vector multipledPolarVector = vectorPolar.MultiplyScalar(scalar);
	cout << "The vector multiplied by " << scalar << " is:" << endl;
	multipledPolarVector.PrintCoordinates();

	cout << "------------------------------------------------------" << endl;

	cout << "The cartesian vector is: " << endl;
	vector.PrintCoordinates();

	Vector dividedVector = vector.DivideScalar(scalar);
	cout << "The vector divided by " << scalar << " is:" << endl;
	dividedVector.PrintCoordinates();

	cout << "The polar vector is: " << endl;
	vectorPolar.PrintCoordinates();

	Vector dividedPolarVector = vectorPolar.DivideScalar(scalar);
	cout << "The vector divided by " << scalar << " is:" << endl;
	dividedPolarVector.PrintCoordinates();

	cout << "------------------------------------------------------" << endl;

	cout << "The cartesian vector is: " << endl;
	vector.PrintCoordinates();

	Vector vector2(1, 1, 1, true);
	cout << "The second cartesian vector is: " << endl;
	vector2.PrintCoordinates();

	cout << "The vector returned after adding by overloading the + operator is:" << endl;	
	Vector addedVector = vector + vector2;
	addedVector.PrintCoordinates();

	cout << "The polar vector is: " << endl;
	vectorPolar.PrintCoordinates();

	cout << "The second cartesian vector is: " << endl;
	vector2.PrintCoordinates();

	cout << "The vector returned after adding by overloading the + operator is:" << endl;
	Vector addedPolarVector = vectorPolar + vector2;
	addedPolarVector.PrintCoordinates();

	cout << "------------------------------------------------------" << endl;

	Vector vector3(1, 1, 1, true);
	cout << "The cartesian vector is: " << endl;
	vector3.PrintCoordinates();

	cout << "The modified vector is: " << endl;
	vector3.Add(vector);
	vector3.PrintCoordinates();

	Vector vector4(1, 1, 1, false);
	cout << "The polar vector is: " << endl;
	vector4.PrintCoordinates();

	cout << "The modified vector is: " << endl;
	vector4.Add(vector);
	vector4.PrintCoordinates();

	cout << "------------------------------------------------------" << endl;

	cout << "The cartesian vector is: " << endl;
	vector.PrintCoordinates();
	cout << "The magnitude is: " << vector.Magnitude() << endl;

	cout << "The polar vector is: " << endl;
	vectorPolar.PrintCoordinates();
	cout << "The magnitude is: " << vectorPolar.Magnitude() << endl;

	cout << "------------------------------------------------------" << endl;

	cout << "The cartesian vector is: " << endl;
	vector.PrintCoordinates();
	cout << "The angle with the x axis is: " << vector.AngleXAxis() << endl;

	cout << "The polar vector is: " << endl;
	vectorPolar.PrintCoordinates();
	cout << "The angle with the x axis is: " << vectorPolar.AngleXAxis() << endl;	

	cout << "------------------------------------------------------" << endl;

	cout << "The cartesian vector is: " << endl;
	vector.PrintCoordinates();
	double angle = 90.0;
	Vector rotatedVector = vector.RotateVector(angle);
	cout << "The vector rotated by " << angle << " is:" << endl;	
	rotatedVector.PrintCoordinates();

	cout << "The polar vector is: " << endl;
	vectorPolar.PrintCoordinates();
	Vector rotatedPolarVector = vectorPolar.RotateVector(angle);
	cout << "The vector rotated by " << angle << " is:" << endl;
	rotatedPolarVector.PrintCoordinates();

	cout << "------------------------------------------------------" << endl;
	
	cout << "The cartesian vector is: " << endl;
	vector.PrintCoordinates();
	cout << "The second cartesian vector is: " << endl;
	vector2.PrintCoordinates();
	cout << "The scalar product between the two is: " << vector.ScalarProduct(vector2) << endl;

	cout << "The polar vector is: " << endl;
	vectorPolar.PrintCoordinates();
	cout << "The second cartesian vector is: " << endl;
	vector2.PrintCoordinates();
	cout << "The scalar product between the two is: " << vectorPolar.ScalarProduct(vector2) << endl;

	cout << "------------------------------------------------------" << endl;

	return 0;
}