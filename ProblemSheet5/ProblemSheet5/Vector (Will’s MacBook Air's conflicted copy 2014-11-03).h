class Vector {
public :
	Vector(double coord1, double coord2, double coord3, bool cartesian); //Cartesian
	~Vector();

	Vector MultiplyScalar(Vector vector, double scalar);
	Vector DivideScalar(Vector vector, double scalar);
	double Magnitude(Vector vector); //Property / private method?
	double AngleXAxis(Vector vector);
	Vector RotateVector(Vector vector, double angle);
	double DotProduct(Vector vector1, Vector vector2);
	Vector Vector::operator+(const Vector &vector); // Need to check syntax

private :

	double _coord1;
	double _coord2;
	double _coord3;
	bool _cartesian;
};

