// Author: Will Parker 

#include <iostream>
#include <cmath>

using namespace std;

void ComputeTriangle(float, float, float);

int main()
{
	float sideA = 10.6;
	float sideC = 7.5;
	float angleA = 57.6;

	ComputeTriangle(sideA, angleA, sideC);

	return 0;
}

void ComputeTriangle(float sideA, float angleA, float sideC)
{
	const double PI = 3.141592653589793238463;

	float radAngleA = (PI / 180.0) * angleA;
	float sineRatioA = sin(radAngleA) / sideA;
	float angleC = asin(sineRatioA * sideC) * 180.0 / PI;
	float angleB = 180 - angleA - angleC;
	float sideB = sin((PI / 180.0) * angleB) * sideA / sin(radAngleA);
	
	cout << "Angle C: " << angleC << endl << "Angle B: " << angleB << endl << "Side B: " << sideB;
}