// Author: Will Parker 

#include <iostream>
#include <cmath>

using namespace std;

int main()
{
	float logBase7To38 = log(38) / log(7);
	cout << "The log to base 7 of 38 is: " << logBase7To38 << endl;
	
	float expo = exp(2.75);
	cout << "e^(2.75) is: " << expo << endl;

	for (int i = 1; i <= 5; i++)
	{
		cout << "x is: " << i << " => e^ln(x) is: " << exp(log(i)) << endl;
	}
	return 0;
}