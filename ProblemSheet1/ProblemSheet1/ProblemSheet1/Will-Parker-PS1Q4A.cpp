// Author: Will Parker 

#include <iostream>
#include <cmath>

using namespace std;

void SolveRealQuadratic(float, float, float);

int main()
{
	cout << "Please enter a, b and c where a is the coefficient of the x squared term, b is the coefficient of the x term and c is the constant (deliminated by spaces or returns):";
	float a = 0.0, b = 0.0, c = 0.0;
	cin >> a >> b >> c;
	SolveRealQuadratic(a, b, c);	
	return 0;
}

void SolveRealQuadratic(float a, float b, float c)
{
	float root1 = (-b + sqrt(pow(b, 2) - 4 * a * c)) / (2 * a);
	float root2 = (-b - sqrt(pow(b, 2) - 4 * a * c)) / (2 * a);
	cout << "The roots are: " << root1 << " and " << root2 << endl;
}