#include "SimulatedParticle.h"
#include "DetectedParticle.h"
#include "Vector4.h"
#include<iostream>
#include "Cuboid.h"
#include "Parallelepiped.h"
#include "Ellipsoid.h"
#include "Sphere.h"
#include "Pyramid.h"

using namespace std;

void PrintShapeStats(Shape &shape)
{
	shape.Name();
	cout << "The surface area is: " << shape.SurfaceArea() << endl;
	cout << "The volume area is: " << shape.Volume() << endl;
	cout << endl;
}

void RunQuestionThree()
{
	cout << "QUESTION 3" << endl;
	double numberOfParticles = 1000;
	double averageSimMass = 0.0;
	double averageDetMass = 0.0;

	for (int i = 0; i < numberOfParticles; i++)
	{
		double mass = 1;
		SimulatedParticle simulatedParticle(mass);
		averageSimMass += sqrt(simulatedParticle.GetInvariant());

		DetectedParticle detectedParticle(simulatedParticle);
		averageDetMass += detectedParticle.GetVectorComponent(0);
	}

	cout << "The simulated particle average mass is: " << averageSimMass / numberOfParticles << endl;  //Always gonna be the same so don't see why I need to calculate this average.
	cout << "The detected particle average mass is: " << averageDetMass / numberOfParticles << endl;

	cout << "-------------------------------------------------------" << endl;
}

void RunQuestionFour()
{
	cout << "QUESTION 4" << endl;

	double side1 = 1.0;
	Cuboid cuboid(side1, side1, side1);
	PrintShapeStats(cuboid);

	double angle = 1.5707; // pi/2 radians
	Parallelepiped parallelepiped(side1, side1, side1, angle);
	PrintShapeStats(parallelepiped);

	Sphere sphere(side1);
	PrintShapeStats(sphere);

	Ellipsoid ellipsoid(side1, side1, side1);
	PrintShapeStats(ellipsoid);

	Pyramid pyramid(side1, side1, 4);
	PrintShapeStats(pyramid);
}

int main()
{
	RunQuestionThree();
	RunQuestionFour();

	return 0;
}