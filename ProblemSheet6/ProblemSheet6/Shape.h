#ifndef SHAPE_H
#define SHAPE_H
class Shape
{
public :
	Shape();
	~Shape();
	virtual double Volume() = 0;
	virtual double SurfaceArea() = 0;
	virtual void Name() = 0;
};
#endif