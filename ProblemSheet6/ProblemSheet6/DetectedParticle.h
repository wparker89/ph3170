#ifndef DETPART_H
#define DETPART_H
#include "SimulatedParticle.h"

class DetectedParticle : public Vector4
{
public :

	DetectedParticle();
	DetectedParticle(SimulatedParticle simulatedParticle);
	double GenerateGaussianDistribution(double mean, double sigma);
	~DetectedParticle();
};
#endif