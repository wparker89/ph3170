#ifndef PARA_H
#define PARA_H
#include "Shape.h"

class Parallelepiped : public Shape
{
public :
	Parallelepiped();
	Parallelepiped(double side1, double side2, double side3, double angle);
	~Parallelepiped();
	double SurfaceArea();
	double Volume();	
	void Name();

private :
	double _side1;
	double _side2;
	double _side3;
	double _angle;
	const double _PI = 3.1415926535897932384626433832795028841971693993751058;
};
#endif