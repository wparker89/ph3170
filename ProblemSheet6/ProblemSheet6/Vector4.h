#ifndef VECTOR4_H
#define VECTOR4_H

class Vector4
{
public : 

	Vector4();
	Vector4(double coord0,  double coord1, double coord2,  double coord3);
	~Vector4();
	double GetVectorComponent(int componenetIndex);
	double GetInvariant();
	
	
protected :

	double _coord0;
	double _coord1;
	double _coord2;
	double _coord3;

};
#endif