#include "Parallelepiped.h"
#include <cmath>
#include <iostream>

Parallelepiped::Parallelepiped() //Special case of cuboid so can inherit cuboid?
{
	_side1 = 1.0;
	_side2 = 1.0;
	_angle = 0.0;
}

Parallelepiped::Parallelepiped(double side1, double side2, double side3, double angle)
{
	_side1 = side1;
	_side2 = side2;
	_side3 = side3;
	if (angle > _PI / 2.0)
	{
		std::cout << "The angle needs to be less than or equal to PI / 2" << std::endl;
		_angle = angle = _PI / 2.0;
	}
	else
	{
		_angle = angle;
	}
	
}

Parallelepiped::~Parallelepiped()
{

}

double Parallelepiped::SurfaceArea()
{
	return _side1 * _side2 * sin(_angle) * 2 + _side3 * _side1 * sin(_angle) * 2 + _side3 * _side2 * sin(_angle) * 2;
}

double Parallelepiped::Volume()
{
	return _side1 * _side2 * _side3 * sin(_angle);
}

void Parallelepiped::Name()
{
	std::cout << "Parallelepiped" << std::endl;
}
