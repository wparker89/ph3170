#include "Vector4.h"
#include<cmath>

Vector4::Vector4()
{
	_coord0 = 0.0;
	_coord1 = 0.0;
	_coord2 = 0.0;
	_coord3 = 0.0;
}

Vector4::Vector4(double coord0, double coord1, double coord2, double coord3)
{
	_coord0 = coord0;
	_coord1 = coord1;
	_coord2 = coord2;
	_coord3 = coord3;
}

Vector4::~Vector4()
{
}

double Vector4::GetVectorComponent(int coordinateIndex) // Maybe enum this?
{
	double component = 0.0;

	switch (coordinateIndex)
	{
	case 0:
		component = _coord0;
		break;
	case 1 :
		component = _coord1;
		break;
	case 2 :
		component = _coord2;
		break;
	case 3 :
		component = _coord3;
		break;
	default:
		component = 0.0; // Unncessary 
		break;
	}
	return component;
}

double Vector4::GetInvariant() //Space time / Energy momentum interval is invariant.
{
	return pow(_coord0, 2) - pow(_coord1, 2) - pow(_coord2, 2) - pow(_coord3, 2);
}