#ifndef PARTICLE_H
#define PARTICLE_H

#include "Vector4.h";

class Particle : public Vector4{

public :

	Particle();

	Particle(double mass, double velocity, double charge, double energy, double momentum, Vector4 vector) : Vector4() 
	{
		// Method body, why set in the header file?
		_time = vector.GetVectorComponent(0);
		_coord1 = vector.GetVectorComponent(1);
		_coord2 = vector.GetVectorComponent(2);
		_coord3 = vector.GetVectorComponent(3);
	};

	~Particle();

	double Mass();
	double Veclocity();
	double Charge();
	double Energy();
	double Momentum();
	double RelGamma();

private :
	
	double _mass;
	double _velocity;
	double _charge;
	double _energy;
	double _momentum;

};
#endif