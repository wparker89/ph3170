#include "SimulatedParticle.h"
#include<cstdlib>
#include<iostream>
#include<ctime>

SimulatedParticle::SimulatedParticle()
{		
	_mass = 1.0;
}

SimulatedParticle::SimulatedParticle(double mass)
{
	srand(time(NULL)); // make it actually random

	_mass = mass;
	double magnitude = GenerateTriangularMomentumDist(1); // Seed value	
	
	// Randomly distribute them, for no reason....
	const double PI = 3.14;
	double random = ((double)rand()) / (double)RAND_MAX; //Get a normalised random value
	double theta = random * cos(magnitude); //is this true random theta?
	double phi = random * PI;

	_coord1 = magnitude * sin(theta) * cos(phi); 
	_coord2 = magnitude * sin(theta) * sin(phi);
	_coord3 = magnitude * cos(theta);
	_coord0 = sqrt(pow(mass, 2) + pow(_coord1, 2) + pow(_coord2, 2) + pow(_coord3, 2));
	
}

SimulatedParticle::~SimulatedParticle()
{
}

double SimulatedParticle::GenerateTriangularMomentumDist(double momentum)
{
	double x = 0.0;
	double y = 0.0;
	do
	{
		x = momentum * rand() / RAND_MAX;
		y = x / momentum;
	} while (1.0 * rand() / RAND_MAX > y);
	return x;
}
