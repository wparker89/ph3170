#include "Pyramid.h"
#include <cmath>
#include <iostream>

Pyramid::Pyramid()
{
	_base = 1.0;
	_height = 1.0;
	_numberBaseSides = 3.0;
}

Pyramid::Pyramid(double base, double height, double numberBaseSides)
{
	_base = base;
	_height = height;
	_numberBaseSides = numberBaseSides;
}

Pyramid::~Pyramid()
{
}

double Pyramid::SurfaceArea()
{
	return _baseArea() + (_basePerimeter() * _slantHeight() / 2.0);
}

double Pyramid::Volume()
{
	return (_numberBaseSides / 12.0) * _height * pow(_base, 2) * 1 / tan(_PI / _numberBaseSides);
}

double Pyramid::_baseArea()
{
	return 1.0 / 2.0 * (_numberBaseSides * _base * _apothem());
}

double Pyramid::_basePerimeter()
{
	return _numberBaseSides * _base;
}

double Pyramid::_slantHeight()
{
	return sqrt(pow(_height, 2) + pow(_base / 2.0, 2));
}

double Pyramid::_apothem()
{
	return 1.0 / 2.0 * (_base * tan(_PI * (_numberBaseSides - 2) / 2.0 * _numberBaseSides));
}

void Pyramid::Name()
{
	std::cout << "Pyramid" << std::endl;
}
