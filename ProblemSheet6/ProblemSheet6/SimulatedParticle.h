#ifndef SIMPART_H
#define SIMPART_H

#include "Vector4.h"

class SimulatedParticle : public Vector4{

public :
	
	SimulatedParticle();
	SimulatedParticle(double mass);
	~SimulatedParticle();
	double GenerateTriangularMomentumDist(double momentum);

private:

	double _mass;
};

#endif