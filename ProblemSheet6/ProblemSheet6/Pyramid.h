#ifndef PYRAM_H
#define PYRAM_H
#include "Shape.h"

class Pyramid: public Shape
{
public:
	Pyramid();
	Pyramid(double base, double height, double numberBaseSides);
	~Pyramid();
	double SurfaceArea();
	double Volume();	
	void Name();

private:
	double _base;
	double _height;
	double _numberBaseSides;
	const double _PI = 3.14;

	double _baseArea();
	double _basePerimeter();
	double _slantHeight();
	double _apothem();
};
#endif