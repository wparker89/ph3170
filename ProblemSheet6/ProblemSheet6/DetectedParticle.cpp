#include "DetectedParticle.h"
#include<iostream>
#include<random>

DetectedParticle::DetectedParticle()
{
}

DetectedParticle::DetectedParticle(SimulatedParticle simulatedParticle)
{
	double mean = simulatedParticle.GetVectorComponent(0);
	const double something = 0.14;
	double sigma = something * sqrt(mean);
	double mass = sqrt(simulatedParticle.GetInvariant());
	double smearedEnergy = 0.0;

	smearedEnergy = GenerateGaussianDistribution(mean, sigma);
	_coord1 = simulatedParticle.GetVectorComponent(1);
	_coord2 = simulatedParticle.GetVectorComponent(2);
	_coord3 = simulatedParticle.GetVectorComponent(3);
	_coord0 = sqrt(pow(smearedEnergy, 2) - pow(_coord1, 2) - pow(_coord2, 2) - pow(_coord3, 2));
}

DetectedParticle::~DetectedParticle()
{
}

double DetectedParticle::GenerateGaussianDistribution(double mean, double sigma)
{
	std::default_random_engine random;
	std::normal_distribution<double> distribution(mean, sigma);
	return distribution(random);
}
