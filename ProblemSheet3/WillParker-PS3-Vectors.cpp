#include<iostream>
#include<cmath>

using namespace std;

double ComputeVectorMagnitudeCartesian(double, double);
double ComputeVectorMagnitudePolar(double, double);
double ComputeVectorMagnitudeCartesian(double, double, double);
double* ConvertCartesianVectorToPolar(double, double);
double* ConvertCartesianVectorToPolar(double, double, double);
double* ConvertPolarVectorToCartesian(double, double);
double* ConvertPolarVectorToCartesian(double, double, double);

int main()
{
  double x = 0.0;
  cout << "Please enter an x component: " << endl;
  cin >> x;

  double y = 0.0;
  cout << "Please enter a y component: " << endl;
  cin >> y;
  cout << "The magnitude of the vector is: " << ComputeVectorMagnitudeCartesian(x, y) << endl;
  double* polar = ConvertCartesianVectorToPolar(x, y);
  cout << "The vector converted to polar coords is: (" << polar[0] << ", " << polar[1] << ")" << endl;
  delete[] polar;

  double r = 0.0;
  cout << "Please enter the r component: " << endl;
  cin >> r;

  double theta = 0.0;
  cout << "Please enter the theta component in radians: " << endl;
  cin >> theta;
  double* cartesian = ConvertPolarVectorToCartesian(r, theta);
  cout << "The vector converted to cartesian coords is: (" << cartesian[0] << ", " << cartesian[1] << ")" << endl;
  delete[] cartesian;
  cout << "The magnitude of the vector is: " << ComputeVectorMagnitudePolar(r, theta) << endl;

  double z = 0.0;
  cout << "Please enter a z component: " << endl;
  cin >> z;
  cout << "The magnitude of the vector with componenets (" << x << ", " << y << ", " << z << ") is: " << ComputeVectorMagnitudeCartesian(x, y, z) << endl;
  double* sphericalPolars = ConvertCartesianVectorToPolar(x, y, z);
  cout << "The same vector in spherical polars is (" << sphericalPolars[0] << ", " << sphericalPolars[1] << ", " << sphericalPolars[2] << ")" << endl;
  delete[] sphericalPolars;

  double phi = 0.0;
  cout << "Please enter a phi component in radians: " << endl;
  cin >> phi;
  cout << "The magnitude of the vector with componenets (" << r << ", " << theta << ", " << phi << ") is: " << ComputeVectorMagnitudePolar(r, theta) << endl;
  double* cartesianCoords = ConvertPolarVectorToCartesian(r, theta, phi);
  cout << "The same vector in spherical polars is (" << cartesianCoords[0] << ", " << cartesianCoords[1] << ", " << cartesianCoords[2] << ")" << endl;
  delete[] cartesianCoords;

  return 0;
}

double ComputeVectorMagnitudeCartesian(double x, double y)
{
  return sqrt(pow(x, 2) + pow(y, 2));
}

double ComputeVectorMagnitudeCartesian(double x, double y, double z)
{
	return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
}

double ComputeVectorMagnitudePolar(double r, double theta)
{
	return r; //??
}

double* ConvertCartesianVectorToPolar(double x, double y, double z)
{
	double r = ComputeVectorMagnitudeCartesian(x, y, z);
	double theta = atan(y / x);
	double phi = acos(z / r); 

	double *polarVector = new double[3] { r, theta, phi };
	return polarVector;
}

double* ConvertCartesianVectorToPolar(double x, double y)
{
	double r = ComputeVectorMagnitudeCartesian(x, y);
	double theta = atan(y / x);

	double *polarVector = new double[2] { r, theta };

	return polarVector;
}

double* ConvertPolarVectorToCartesian(double r, double theta)
{
  double x = 0.0;
  double y = 0.0;

  x = r * cos(theta);
  y = r * sin(theta);

  double *vector = new double[2] {x, y};
  return vector;
}

double* ConvertPolarVectorToCartesian(double r, double theta, double phi)
{
	double x = 0.0;
	double y = 0.0;
	double z = 0.0;

	x = r * sin(theta) * cos(phi);
	y = r * sin(theta) * sin(phi);
	z = r * cos(theta);

	double *vector = new double[3] { x, y, z };
	return vector;
}
  
