#include<iostream>
#include<cmath>

using namespace std;

double Differentiate(double, double, double, double, double); // Consider removing.
double DifferentiateQuadratic(double, double, double);
double ComputeQuadratic(double, double, double, double);
double ComputeRootsNewtonRaphson(double, double, double, double);
double ComputeRootsRecursiveNewtonRaphson(double, double, double, double, double);
double ComputeRootsRecursiveNewtonRaphson(double, double[], int);

int main()
{
	//How to determine seed values?  Hardcoded or ask user input?
  double quadSeedValue1 = -1.0;
  double quadSeedValue2 = 3;
  double quadRoot1 = ComputeRootsNewtonRaphson(quadSeedValue1, 6, -17, -14); // Refactor to take array of coefficients.
  double quadRoot2 = ComputeRootsNewtonRaphson(quadSeedValue2, 6, -17, -14);
  cout << "The roots are: " << quadRoot1 << " and " << quadRoot2 << endl;

  double coefficients[3] = { 6, -17, -14 };
  double quadRecurRoot1 = ComputeRootsRecursiveNewtonRaphson(quadSeedValue1, coefficients, 2);
  double quadRecurRoot2 = ComputeRootsRecursiveNewtonRaphson(quadSeedValue2, coefficients, 2);
  cout << "The roots recursive are: " << quadRecurRoot1 << " and " << quadRecurRoot2 << endl;

  double cubSeedValue1 = -3.5;
  double cubSeedValue2 = -0.5;
  double cubSeedValue3 = 1.0;
  double cubCoefficients[4] = { 2, 7, 2, -3 };
  double cubRoot1 = ComputeRootsRecursiveNewtonRaphson(cubSeedValue1, cubCoefficients, 3);
  double cubRoot2 = ComputeRootsRecursiveNewtonRaphson(cubSeedValue2, cubCoefficients, 3);
  double cubRoot3 = ComputeRootsRecursiveNewtonRaphson(cubSeedValue3, cubCoefficients, 3);
  cout << "The roots recursive are: " << cubRoot1 << ", " << cubRoot2 << " and " << cubRoot3 << endl;

  double cub2SeedValue1 = -0.5;
  double cub2SeedValue2 = -1.5;
  double cub2SeedValue3 = 3.0;
  double cub2Coefficients[4] = { 15, -3, -42, -24 };
  double cub2Root1 = ComputeRootsRecursiveNewtonRaphson(cub2SeedValue1, cub2Coefficients, 3);
  double cub2Root2 = ComputeRootsRecursiveNewtonRaphson(cub2SeedValue2, cub2Coefficients, 3);
  double cub2Root3 = ComputeRootsRecursiveNewtonRaphson(cub2SeedValue3, cub2Coefficients, 3);
  cout << "The roots recursive are: " << cub2Root1 << ", " << cub2Root2 << " and " << cub2Root3 << endl;

  return 0;
}

//double Differentiate(double x, double dx, double a, double b, double c)
//{
//  double derivative = 0.0;
//
//  derivative = (ComputeQuadratic(a, b, c, (x + dx)) - ComputeQuadratic(a, b, c, x)) / dx;
//
//  return derivative;
//}

double DifferentiateQuadratic(double x, double a, double b)
{
	return 2 * a * x + b;
}

double DifferentiateCubic(double x, double a, double b, double c)
{
	return 3 * a * pow(x, 2) + 2 * b * x + c;
}

double ComputeQuadratic(double x, double a, double b, double c)
{
  double quadratic = a * pow(x, 2) + b * x + c; 
  return  quadratic;
}

double ComputeCubic(double x, double a, double b, double c, double d)
{
	return a * pow(x, 3) + b * pow(x, 2) + c * x + d;
}

double ComputeRootsNewtonRaphson(double x, double a, double b, double c)
{  
  double nextIteration = x - ComputeQuadratic(x, a, b, c) / DifferentiateQuadratic(x, a, b);
  
  while(nextIteration - x > 0.1)
    {
      x = nextIteration;
      nextIteration = x - ComputeQuadratic(nextIteration, a, b, c) / DifferentiateQuadratic(nextIteration, a, b);
    }
  
  return nextIteration;
}

double ComputeRootsRecursiveNewtonRaphson(double x, double coefficients[], int order)
{
  double nextIteration = 0.0;

	switch (order)
	{
	case 2:
		nextIteration = x - ComputeQuadratic(x, coefficients[0], coefficients[1], coefficients[2]) / DifferentiateQuadratic(x, coefficients[0], coefficients[1]);
		if (nextIteration - x > 0.1)
		{
			ComputeRootsRecursiveNewtonRaphson(nextIteration, coefficients, order);
			x = nextIteration;
		}
		break;
	case 3:
		nextIteration = x - ComputeCubic(x, coefficients[0], coefficients[1], coefficients[2], coefficients[3]) / DifferentiateCubic(x, coefficients[0], coefficients[1], coefficients[2]);
		if (nextIteration - x > 0.1)
		{
			ComputeRootsRecursiveNewtonRaphson(nextIteration, coefficients, order);
			x = nextIteration;
		}
		break;
	}
	
	return nextIteration;
}
