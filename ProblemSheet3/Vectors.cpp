#include<iostream>
#include<cmath>

using namespace std;

double ComputeVectorMagnitudeCartesian(double, double);
double ComputeVectorMagnitudePolar(double, double);
double ComputeVectorMagnitudeCartesian(double, double, double);
double* ConvertCartesianVectorToPolar(double, double);
double* ConvertPolarVectorToCartesian(double, double);

int main()
{
  double x_comp = 0.0;
  cout << "Please enter an x component: " << endl;
  cin >> x_comp;

  double y_comp = 0.0;
  cout << "Please enter a y component: " << endl;
  cin >> y_comp;
  cout << "The magnitude of the vector is: " << ComputeVectorMagnitudeCartesian(x_comp, y_comp) << endl;

  double r = 0.0;
  cout << "Please enter the r component: " << endl;
  cin >> r;

  double theta = 0.0;
  cout << "Please enter the theta component: " << endl;
  cout << "Polar to cartestian is: " << ComputeVectorMagnitudePolar(r, theta) << endl;
  return 0.0;
}

double ComputeVectorMagnitudeCartesian(double x, double y)
{
  return sqrt(pow(x, 2) + pow(y, 2));
}

double ComputeVectorMagnitudeCartesian(double x, double y, double z)
{
	return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
}

double ComputeVectorMagnitudePolar(double r, double theta)
{
	return r; //??
}

double* ConvertCartesianVectorToPolar(double x, double y, double z)
{
	double r = ComputeVectorMagnitudeCartesian(x, y);
	double theta = acos(z / r);
	double phi = atan(y / x);

	double polarVector[3] = { r, theta, phi };
	return polarVector;
}

double* ConvertCartesianVectorToPolar(double x, double y)
{
	double r = ComputeVectorMagnitudeCartesian(x, y);
	double theta = atan(y / x); // Need to do some sign checking.

	double polarVector[2] = { r, theta };
	return polarVector;
}

double* ConvertPolarVectorToCartesian(double r, double theta)
{
  double x = 0.0;
  double y = 0.0;
  x = r * cos(theta);
  y = r * sin(theta);
  double vector[2] = {x, y};
  return vector;
}

double* ConvertPolarVectorToCartesian(double r, double theta, double phi)
{
	double x = 0.0;
	double y = 0.0;
	double z = 0.0;
	x = r * sin(theta) * cos(phi);
	y = r * sin(theta) * sin(phi);
	z = r * cos(theta);
	double vector[3] = { x, y, z };
	return vector;
}
  
