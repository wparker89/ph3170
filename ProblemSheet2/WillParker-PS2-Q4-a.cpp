//#include<iostream>
//
//using namespace std;
//
//int sumNumbersFromRangeWithFor(int, int);
//int sumNumbersFromRangeWithWhile(int, int);
//
//int main()
//{
//  int start = 0;
//  int end = 0;
//  do
//    {
//      cout << "Please enter a starting number: " << endl;
//      cin >> start;
//      cout << "Please enter an ending number: " << endl;
//      cin >> end;
//      
//      if (start >= end)
//	{
//	  cout << "Please make sure starting number is less than ending number." << endl;  
//	}
//    }
//  while (start >= end);
//
//  cout << "The sum of all the numbers inclusive from " << start << " to " << end << " is: " << sumNumbersFromRangeWithFor(start, end) << endl;
//  cout << "The sum of all the numbers inclusive from " << start << " to " << end << " is: " << sumNumbersFromRangeWithWhile(start, end) << endl;
//  return 0;
//}
//
//int sumNumbersFromRangeWithFor(int start, int end)
//{
//  int sum = 0;
//  for (int i = start; i <= end; i++)
//    {
//      sum += i;
//    }
//  return sum;
//}
//
//int sumNumbersFromRangeWithWhile(int start, int end)
//{
//  int sum = 0;
//  int i = start;
//  while (i <= end)
//    {
//      sum += i;
//      i++;
//    }
//  return sum;
//}
