#include<iostream>
#include<cmath>
#include<string>

using namespace std;

bool isLeapYear(int);
float calculateDaysToYear(int);
int sumDaysInMonthRange(int, bool);

int main()
{
  int year = 0;
  int month = 0;
  int day = 0;

  // Error handling?
  cout << "Please enter a year: " << endl;
  cin >> year;
  cout << "Please enter a month: " << endl;
  cin >> month;
  cout << "Please enter a day: " << endl;
  cin >> day;

  bool leapYear = isLeapYear(year);
  float totalDays = 0;

  totalDays += calculateDaysToYear(year);
  totalDays += sumDaysInMonthRange(month, leapYear);
  if (day != 1)
    {
      totalDays += day;
    }
  if (year != 2000 && leapYear)
   {
    totalDays += 1;
   }
  
  int dayIdentifier = (int)floor(totalDays) % 7;
  string days [7] = { "Saturday", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday" };
  cout << "The day will be a " << days[dayIdentifier] << "." << endl;
    
  return 0;
}

int sumDaysInMonthRange(int month, bool isLeapYear)
{
  int days = 0;
  for (int i = 1; i < month; i++)
    {
      if (i == 9 || i == 4 || i == 6 || i == 11)
	{
	  days += 30;
	}
      else if (i == 1 || i == 3 || i == 5 || i == 7 || i == 8 || i == 10 || i == 12)
	{
	  days += 31;
	}
      else if (i == 2)
	{
	  if (isLeapYear)
	    {
	      days += 29;
	    }
	  else
	    {
	      days += 28;
	    }
	}
    }
  return days;
}

float calculateDaysToYear(int year)
{
  const float daysInYear = 365;
  float totalDays = 0.0;

  for (int i = 2000; i < year - 1; i++)
  {
	  if (isLeapYear(i))
		  totalDays += daysInYear;
	  else
		  totalDays += daysInYear + 1;
  }
  return totalDays;
}

bool isLeapYear(int year)
{
  bool isLeapYear = false;

  if (year % 4 == 0)
    {
      isLeapYear = true;
      if (year % 100 == 0)
	{
	  isLeapYear = false;
	  if (year % 400 == 0)
	    {
	      isLeapYear = true;
	    }
	}
    }
  return isLeapYear;
}
