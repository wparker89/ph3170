#include<iostream>

using namespace std;

double Minor(double**, int, int, int);
double Determinant(double**, int);
double** Cofactors(double**, int);
double** Transpose(double**, int);
double** Inverse(double**, int);

double Determinant(double** matrix, int size)
{
	double determinant = 0.0;
	if (size == 2)
	{
		determinant = (matrix[1][1] * matrix[0][0]) - (matrix[0][1] * matrix[1][0]);
	}
	else
	{
		for (int i = 0; i < size; i++)
		{
			determinant += pow(-1, i + 2) * matrix[0][i] * Minor(matrix, 1, i + 1, size);
		}
	}
	return determinant;
}

double Minor(double** matrix, int row, int col, const int size)
{
	double determinant = 0.0;
	if (size == 2) // Can compute det easily
	{
		determinant = Determinant(matrix, size);
	}
	else if (size > 2)
	{
		double** minor = new double*[size - 1];

		for (int i = 0; i < size - 1; i++)
		{
			minor[i] = new double[size - 1]; // allocating 2nd pointer size
		}

		int colIndex = 0;
		int rowIndex = 0;

		for (int i = 0; i < size; i++)
		{
			if (i != row - 1) // Delete selected row
			{
				for (int j = 0; j < size; j++)
				{
					if (j != col - 1) // Delete selected col
					{
						minor[rowIndex][colIndex] = matrix[i][j]; // Add to the minor						
						colIndex++;
					}
				}
				rowIndex++;
				colIndex = 0;
			}
		}
		determinant = Determinant(minor, size - 1);
		for (int i = 0; i < size - 1; i++)
		{
			delete[] minor[i];
		}
		delete[] minor;
	}
	else
	{
		cout << "Error occurred";
		return 0; // some error
	}
	return determinant;
}

double** Cofactors(double** matrix, const int size)
{
	double** cofactors = new double*[size];

	for (int i = 0; i < size; i++)
	{
		cofactors[i] = new double[size]; // allocating 2nd pointer size
	}

	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			cofactors[i][j] = pow(-1, i + j + 2) * Minor(matrix, i + 1, j + 1, size);
		}
	}
	return cofactors;
}

double** Transpose(double** matrix, int size)
{
	double** transpose = new double*[size];

	for (int i = 0; i < size; i++)
	{
		transpose[i] = new double[size]; // allocating 2nd pointer size
	}

	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			transpose[i][j] = matrix[j][i];
		}
	}
	return transpose;
}

double** Inverse(double** matrix, int size)
{
	double determinant = Determinant(matrix, size);
	double** cofactors = Cofactors(matrix, size);
	double** transpose = Transpose(cofactors, size);
	double** inverse = new double*[size];

	for (int i = 0; i < size; i++)
	{
		inverse[i] = new double[size]; // allocating 2nd pointer size
	}

	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			inverse[i][j] = transpose[i][j] / determinant;
		}
	}
	for (int i = 0; i < size; i++)
	{
		delete[] transpose[i];
		delete[] cofactors[i];
 	}
	delete[] transpose;
	delete[] cofactors;
	
	return inverse;
}

int main()
{
	double** matrix = new double*[3];

	for (int i = 0; i < 3; i++)
	{
		matrix[i] = new double[3]; // allocating 2nd pointer size
	}

	// Read in from a text file?
	matrix[0][0] = 5;
	matrix[0][1] = 7;
	matrix[0][2] = 3;
	
	matrix[1][0] = 1;
	matrix[1][1] = 8;
	matrix[1][2] = 4;

	matrix[2][0] = 2;
	matrix[2][1] = 7;
	matrix[2][2] = 6;

	double** inverse = Inverse(matrix, 3);

	cout << "The matrix is" << endl;

	for (int i = 0; i < 3; i++)
	{
		cout << matrix[i][0] << " " << matrix[i][1] << " " << matrix[i][2] << endl;
	}

	cout << endl << "The inverse is" << endl;

	for (int i = 0; i < 3; i++)
	{
		cout << inverse[i][0] << " " << inverse[i][1] << " " << inverse[i][2] << endl;
	}

	cout << endl;
	double temp = 0.0;
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			for (int k = 0; k < 3; k++)
			{
				temp += matrix[i][k] * inverse[k][j];
			}
			cout << fixed << temp << " ";
			temp = 0.0;
		}
		cout << endl;
	}
	
	for (int i = 0; i < 3; i++)
	{
		delete[] matrix[i];
		delete[] inverse[i];		
	}
	delete[] matrix;
	delete[] inverse;

	getchar(); // stop console from closing
}