#include<iostream>
#include<cmath>

using namespace std;

double* ComputeCofactors(double[][3], int);
double ComputeDeterminant(double[][2], int);

double ComputeDeterminant(double matrix[][2], int size)
{
  double determinant = 0.0;

  determinant = matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0];

  return determinant;
}

double* ComputeCofactors(double matrix[][3], int size)
{
  for (int i = 0; i < size; i++)
    {
      for (int j = 0; j < size; j++)
	{
      
	  double sign = pow(-1, i + j);

	  if (i == 0)
	    {
	      double minor[2][2] = { { matrix[i + 1][j +1], matrix[i + 1][j + 2] }, { matrix[i + 2][j + 1], matrix[i + 2][j + 2] } };
	    }
	  if (i == 1)
	    {
	      double minor[2][2] = { { matrix[i - 1][j +1], matrix[i - 1][j + 2] }, { matrix[i + 1][j + 1], matrix[i + 1][j + 2] } };
	    }
	  if (i == 2)
	    {
	      double minor[2][2] = { { matrix[i - 2][j + 1], matrix[i - 2][j + 2] }, { matrix[i - 2][j + 1], matrix[i - 2][j + 2] } };
	    }
	}
    }
}

int main()
{
  int size = 3;
  double matrix[3][3] = { {5, 7, 3}, {1, 8, 4}, {2, 7, 6} }; // Need a way to make matrix size variable.  Stick with 3x3 for now.

  ComputeCofactors(matrix, size);
  return 0;
}
